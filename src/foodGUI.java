import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodGUI {
    private JPanel sumsum;
    private JButton umeButton;
    private JButton syakeButton;
    private JButton sioButton;
    private JTextArea textArea1;
    private JButton tunaButton;
    private JButton kelpButton;
    private JButton roeButton;
    private JLabel item;
    private JLabel topLabel;
    private JButton check;
    private JLabel total;
    private JLabel yen;
    private JTabbedPane tabbedPane1;
    private JButton karaageButton;
    private JButton tamagoButton;
    private JButton takuanButton;
    private JButton cucumber;
    private JButton tonjirubutton;
    private JButton wakamebutton;
    private JButton mizubutton;
    private JButton greenTeaButton;
    private JButton tofubutton;
    private JButton mugityabutton;
    private JButton cancelbutton;

    int sum = 0;

    void order(String food , int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+ food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            String currentText = textArea1.getText();
            JOptionPane.showMessageDialog(null,"Thank you for ordering "
                    + food + "! It will be served as soon as possible.");
            textArea1.setText(currentText + food + " " + price + " yen" + "\n");
            sum +=price;
            yen.setText(sum + " yen");
        }
    }
public foodGUI() {
    umeButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Ume", 200);
        }
    });
    umeButton.setIcon(new ImageIcon(
            this.getClass().getResource("ume.jpg")
    ));
    syakeButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Salmon" , 210);
        }
    });
    syakeButton.setIcon(new ImageIcon(
            this.getClass().getResource("syake.jpg")
    ));
    sioButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Salt" , 150);
        }
    });
    sioButton.setIcon(new ImageIcon(
            this.getClass().getResource("sio.jpg")
    ));
    tunaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Tuna mayo" , 180);
        }
    });
    tunaButton.setIcon(new ImageIcon(
            this.getClass().getResource("tunamayo.jpg")
    ));
    kelpButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Kelp" , 185);
        }
    });
    kelpButton.setIcon(new ImageIcon(
            this.getClass().getResource("konbu.jpg")
    ));
    roeButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Cod roe" , 350);
        }
    });
    roeButton.setIcon(new ImageIcon(
            this.getClass().getResource("mentai.jpg")
    ));

    check.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to check out?","Check out Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0){
                if(sum == 0){
                    int option = JOptionPane.showConfirmDialog(null, "No dish selected!",
                            "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                }
                else {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + sum + " yen");
                    sum = 0;
                    textArea1.setText("");
                    yen.setText("0 yen");
                }
            }
        }
    });
    karaageButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
                order("Karaage" , 280);
        }
    });
    karaageButton.setIcon(new ImageIcon(
            this.getClass().getResource("karaage.jpg")
    ));
    tamagoButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
                order("Omelet" , 230);
        }
    });
    tamagoButton.setIcon(new ImageIcon(
            this.getClass().getResource("tamagoyaki.jpg")
    ));
    takuanButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Pickled radish" , 120);
        }
    });
    takuanButton.setIcon(new ImageIcon(
            this.getClass().getResource("takuan.jpg")
    ));
    cucumber.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Pickled cucumber" , 110);
        }
    });
    cucumber.setIcon(new ImageIcon(
            this.getClass().getResource("kyuuri.jpg")
    ));
    tonjirubutton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Pork and vegetables" , 300);
        }
    });
    tonjirubutton.setIcon(new ImageIcon(
            this.getClass().getResource("tonjiru.jpg")
    ));
    wakamebutton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Seaweed" , 170);
        }
    });
    wakamebutton.setIcon(new ImageIcon(
            this.getClass().getResource("wakame.jpg")
    ));
    tofubutton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Tofu" , 175);
        }
    });
    tofubutton.setIcon(new ImageIcon(
            this.getClass().getResource("tofu.jpg")
    ));
    mizubutton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Water" , 0);
        }
    });
    mizubutton.setIcon(new ImageIcon(
            this.getClass().getResource("mizu.jpg")
    ));
    greenTeaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Green tea" , 15);
        }
    });
    greenTeaButton.setIcon(new ImageIcon(
            this.getClass().getResource("ryokutya.jpg")
    ));
    mugityabutton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Barley tea" , 10);
        }
    });
    mugityabutton.setIcon(new ImageIcon(
            this.getClass().getResource("mugitya.jpg")
    ));
    cancelbutton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int confirm = JOptionPane.showConfirmDialog(null,
                    "Would you like to cancel this order?","Cancel Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirm == 0){
                    JOptionPane.showMessageDialog(null,
                            "Then start ordering again");
                    sum = 0;
                    textArea1.setText("");
                    yen.setText("0 yen");
            }
        }
    });
}


    public static void main(String[] args) {
        JFrame frame = new JFrame("foodGUI");
        frame.setContentPane(new foodGUI().sumsum);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
